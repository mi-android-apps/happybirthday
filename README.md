# Birthday Card Sample App
Single-screen android app that displays a birthday card.

### Objectives
* Learn how to place layouts on a page to crate images, buttons, and text on the phone screen.
* Learn how grouping layouts together allows for more creative and original designs.

### Build Instructions
This sample uses the Gradle build system. To build this project, use the
"gradlew build" command or use "Import Project" in Android Studio.

### Screenshots
![please find images under app-screenshots directory](app-screenshots/screen-1.png "Birthday Card")

